# AUS_WA_KoombanaBay

Biogeochemical characterisation of marine flows within Koombana Bay, Western Australia, 
prior to further Bunbury Outer-Inner Harbour Port expansions post-2022 

Roadmap

visual: multi-D map of Koombana Bay topography
cartography: list activity (sediment dumping) transects for practical application (fishing, diving, observing)


Files

terrain.Rmd - geography, investigation


Description
PhD proposal

Source / Resources

Visuals


Contributing
I am 'cleaning up' all my projects/files towards publishing a paper, allowing me to apply for further studies and grants etc.. all help is appreciated.

Authors and acknowledgment
Lets work together!

License
Private invitation collaborations towards publishing, but always open to sharing techniques/code.

Project status
Ongoing
